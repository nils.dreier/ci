FROM ubuntu:20.04
MAINTAINER simon.praetorius@tu-dresden.de
RUN rm -f /etc/apt/apt.conf.d/docker-gzip-indexes \
  && rm -rf /var/lib/apt/lists/*

RUN export DEBIAN_FRONTEND=noninteractive; \
  apt-get update \
  && apt-get dist-upgrade --no-install-recommends --yes \
  && apt-get install --no-install-recommends --yes \
  autoconf \
  automake \
  bison \
  build-essential \
  ca-certificates \
  clang clang-9 clang-10 \
  cmake \
  coinor-libipopt-dev \
  curl \
  flex \
  g++ g++-9 g++-10 \
  gcc gcc-9 gcc-10 \
  gfortran gfortran-9 gfortran-10 \
  git \
  git-lfs \
  gnuplot-nox \
  libadolc-dev \
  libalberta-dev \
  libarpack++2-dev \
  libboost-dev \
  libboost-program-options-dev \
  libboost-serialization-dev \
  libboost-system-dev \
  libeigen3-dev \
  libffi-dev \
  libgmp-dev \
  libgtest-dev \
  libisl-dev \
  libltdl-dev \
  libmpfr-dev \
  libmpfrc++-dev \
  libmuparser-dev \
  libopenblas-dev \
  libscotchmetis-dev \
  libscotchparmetis-dev \
  libsuitesparse-dev \
  libsuperlu-dev \
  libtinyxml2-dev \
  libtiff-dev \
  libtool \
  locales-all \
  mpi-default-bin \
  mpi-default-dev \
  ninja-build \
  openssh-client \
  pkg-config \
  petsc-dev \
  python3 \
  python3-dev \
  python3-matplotlib \
  python3-mpi4py \
  python3-numpy \
  python3-pip \
  python3-pytest \
  python3-setuptools \
  python3-scipy \
  python3-ufl \
  python3-venv \
  python3-vtk7 \
  python3-wheel \
  vc-dev \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

RUN adduser --disabled-password --home /duneci --uid 50000 duneci

# The environment for Dune builds is set up in base-common/dune-setup.dockerfile,
# which gets appended to this file during the build process
