# stable/Dockerfile
#
# ****************************************************************************************************
# This file is a verbatim copy of
# https://gitlab.com/smuething/gitlab-runner-podman-executor/raw/master/images/buildah/Dockerfile
# ****************************************************************************************************
#
# Build a Buildah container image from the latest
# stable version of Buildah on the Fedoras Updates System.
# https://bodhi.fedoraproject.org/updates/?search=buildah
# This image can be used to create a secured container
# that runs safely with privileges within the container.
#

# The version of fuse-verlayfs currently shipped in Fedora 31 has problems when extracting
# large amounts of data, causing builds inside the container to fail. So we manually build
# a statically linked version of fuse-overlayfs and copy it into the final container. The
# first part of this Dockerfile is mostly copied from
# https://raw.githubusercontent.com/containers/fuse-overlayfs/master/Dockerfile.static
# The main changes are shallow cloning and selecting a specific ref when cloning
# fuse-overlayfs.

ARG FUSE_OVERLAYFS_REF=v0.6.5

FROM registry.fedoraproject.org/fedora:latest AS fuse-overlayfs

WORKDIR /build
RUN dnf --setopt=install_weak_deps=False update -y && \
    dnf --setopt=install_weak_deps=False install -y git make automake autoconf gcc glibc-static meson ninja-build clang

RUN git clone --depth 1 https://github.com/libfuse/libfuse && \
    cd libfuse && \
    mkdir build && \
    cd build && \
    LDFLAGS="-lpthread" meson --prefix /usr -D default_library=static .. && \
    ninja && \
    ninja install

RUN git clone --depth=1 -b ${FUSE_OVERLAYFS_REF} https://github.com/containers/fuse-overlayfs && \
    cd fuse-overlayfs && \
    sh autogen.sh && \
    LIBS="-ldl" LDFLAGS="-static" ./configure --prefix /usr && \
    make && \
    make install

FROM registry.fedoraproject.org/fedora:latest

# Don't include container-selinux and remove
# directories used by dnf that are just taking
# up space.
RUN dnf --setopt=install_weak_deps=False -y update && \
    dnf --setopt=install_weak_deps=False -y install buildah fuse-overlayfs skopeo --exclude container-selinux && \
    rm -rf /var/cache /var/log/dnf* /var/log/yum.*

# Adjust storage.conf to enable Fuse storage.
RUN sed -i -e 's|^#mount_program|mount_program|g' -e '/additionalimage.*/a "/var/lib/shared",' /etc/containers/storage.conf
RUN mkdir -p /var/lib/shared/overlay-images /var/lib/shared/overlay-layers; touch /var/lib/shared/overlay-images/images.lock; touch /var/lib/shared/overlay-layers/layers.lock

# Copy working version of fuse-overlayfs
COPY --from=fuse-overlayfs /usr/bin/fuse-overlayfs /usr/bin/fuse-overlayfs

# Set up environment variables to note that this is
# not starting with usernamespace and default to
# isolate the filesystem with chroot.
# Also, build Docker images by default for better compatibility
ENV _BUILDAH_STARTED_IN_USERNS="" BUILDAH_ISOLATION=chroot BUILDAH_FORMAT=docker
